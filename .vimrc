" ----- Vim -----
set termguicolors   " Use Gui color settings   
set nocompatible    " 
set wrap            " Wrap lines
set number          " Show line number
set laststatus=2    " 
set noshowmode      "
set cmdheight=1     " Give more space for displaying messages.
set relativenumber  " Sets line numbers to be relative
" ---- Use Tab ---- "
set smartindent     "
set noexpandtab     " Always uses tabs instead of space characters (noet).
set tabstop=4       " The width of a TAB is set to 4.
set shiftwidth=4    " Indents will have a width of 4

" ----- Import Plugins File ----- 
if filereadable(expand("~/.vimrc.plug"))
	source ~/.vimrc.plug
endif

augroup enfocado_customization
  autocmd!
    autocmd ColorScheme enfocado ++nested highlight Normal ctermbg=NONE guibg=NONE
    autocmd ColorScheme enfocado ++nested highlight NormalNC ctermbg=NONE guibg=NONE
    autocmd ColorScheme enfocado ++nested highlight LineNr ctermbg=NONE guibg=NONE guifg=#bbc2cf
augroup END

" ---- Enfocado Colorscheme Settings ---- 
colorscheme enfocado " Set colorscheme


" ----- Cursor -----
let &t_SI = "\e[6 q" " Line cursor when in INSERT mode
let &t_EI = "\e[2 q" " Block cursor when in NORMAL mode

" ----- Lightline ----- 
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

" ----- Fuzzy Finder -----
nnoremap <C-p> :Files<Cr>
nnoremap <C-j> :Buffers<Cr>
let g:fzf_layout = { 'down': '30%' } " Stick layout to bottom with 30% height
let g:fzf_preview_window = []        " Dont show preview window
