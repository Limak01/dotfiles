# Save commands history
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=1000
setopt SHARE_HISTORY

# Export path to user bin folder if exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# Spicetify path
export PATH=$PATH:/home/kamil/.spicetify

# Use vim as main editor
export EDITOR='nvim' 

### ALIASES ###
# Git bare repo alias for dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# ls to lsd 
alias ls='lsd --group-directories-first'
alias ll='lsd -lA --group-directories-first'

# System
alias update='yay -Syu'
alias pmi='sudo pacman -S'
alias pmr='sudo pacman -Rns'
alias auri='yay -S'
alias aurr='yay -Rcns'

# vim
alias vim='nvim'
alias v='nvim'

# set feh to open images in fullscreen
alias feh='feh --scale-down -g 1280x720 -d -S filename'

# cd to zoxide
alias cd='z'

# Plugins
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Keybind ctrl+p to run fzf using zoxide
run_fzf() {
    local dir="$(eval "zoxide query -i")"

    if [[ -z "$dir" ]]; then
        zle redisplay
        return 0
    fi

    zle push-line
    BUFFER="cd ${(q)dir}"
    zle accept-line
    local ret=$?
    unset dir
    zle reset-prompt
    return $ret
}

zle -N run_fzf 
bindkey '^p' run_fzf

# Start starship prompt
eval "$(starship init zsh)"
# Start zoxide
eval "$(zoxide init zsh)"
