#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Add bin folder to PATH
[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"

### EXPORTS ###
# Expand the history size
export HISTFILESIZE=10000
export HISTSIZE=500
export HISTTIMEFORMAT="%F %T" # add timestamp to history

# Don't put duplicate lines in the history and do not add lines that start with a space
export HISTCONTROL=erasedups:ignoredups:ignorespace

# Use vim as main editor
export EDITOR='nvim'

### ALIASES ###

# Git bare repo alias for dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# ls to lsd 
alias ls='lsd --group-directories-first'
alias ll='lsd -lA --group-directories-first'

alias update='yay -Syu'
alias install='sudo pacman -S'
alias remove='sudo pacman -Rns'
alias aurinstall='yay -S'
alias aurremove='yay -Rcns'

# vim
alias vim='nvim'
alias v='nvim'

### FUNCTIONS ###

extract() {
    for archive in "$@"; do
		if [ -f "$archive" ]; then
			case $archive in
			*.tar.bz2) tar xvjf $archive ;;
			*.tar.gz) tar xvzf $archive ;;
			*.bz2) bunzip2 $archive ;;
			*.rar) rar x $archive ;;
			*.gz) gunzip $archive ;;
			*.tar) tar xvf $archive ;;
			*.tbz2) tar xvjf $archive ;;
			*.tgz) tar xvzf $archive ;;
			*.zip) unzip $archive ;;
			*.Z) uncompress $archive ;;
			*.7z) 7z x $archive ;;
			*) echo "Don't know how to extract '$archive'..." ;;
			esac
		else
			echo "'$archive' is not a valid file!"
		fi
	done
}

### KEYBINDS ###
bind '"\C-f":"zi\n"'

if command -v fzf &> /dev/null; then
    eval "$(fzf --bash)"
fi

eval "$(zoxide init bash)"

color_capabilites=$(tput colors 2> /dev/null)

if [ $color_capabilites -eq 256 ]; then
    PS1=" ( \e[38;5;202m\u\e[0m ) [ \e[38;5;153m\W\e[0m ] \e[38;5;118m\e[0m "
else
    PS1="( \e[32m\u\e[0m ) [ \e[35m\W\e[0m ] \e[33m>\e[0m "
fi

